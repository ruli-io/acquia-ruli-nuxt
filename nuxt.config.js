module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Raul Salido | Let\'s build something great together',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Consulting, Design and Development solutions for your next big digital experience.' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/icon.png' },
      { rel: 'stylesheet', href: 'https://use.typekit.net/vex0hvx.css' },
    ],
    script: [
      { src: '/drift.js' }
    ]
  },
  /*
  ** Loading
  */
  loading: false,
  /*
  ** Modules
  */
  modules: [
    ['nuxt-sass-resources-loader', [
      '@/assets/scss/globals/resources.scss'
    ]],
    ['@nuxtjs/google-tag-manager', {
      id: 'GTM-N6XG46M',
      pageTracking: true
    }],
  ],
  /*
  ** Plugins
  */
  plugins: [
    { src: '~/plugins/vue-mq', ssr: false },
    { src: '~/plugins/vue-scrollmagic', ssr: false },
  ],
  /*
  ** CSS
  */
  css: [
    '@/assets/scss/main.scss'
  ],
  /*
  ** Build configuration
  */
  build: {
    postcss: {
      plugins: {
        'postcss-cssnext': {
          features: {
            customProperties: false
          }
        }
      }
    },
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
