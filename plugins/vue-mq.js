import Vue from 'vue'
import VueMq from 'vue-mq'

if (process.browser) {

  Vue.use(VueMq, {
    breakpoints: {
      mobile: 768,
      tablet: 1087,
      desktop: 1279,
      widescreen: 1473,
      fullhd: Infinity
    }
  })

}
