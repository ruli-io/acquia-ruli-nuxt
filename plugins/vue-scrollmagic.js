import Vue from 'vue'
import KsVueScrollmagic from 'ks-vue-scrollmagic'

if (process.browser) {

  Vue.use(KsVueScrollmagic)

}
