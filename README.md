# Acquia Showcase - Ruli.io
---
## Overview

- The Nuxt framework allowed me to build a contemporary and fast static site to showcase my consulting services.
- Vue is great for component based design. I chose a technology that would make it easy to update my sitemap and add/remove components and pages quickly. Given I wanted a short scope, and because I can code, there was no need for a CMS.
- Nuxt also made hosting options easy (and free). Generating your static Nuxt site through NPM is instant, and the HTML output can be hosted in a free platform. I chose Netlify, as their continuous deployment is practical and they provide free SSL.

## Key Features

- /pages - Contains the various pages for the site. These are .vue files that house HTML, JS, and SASS.
- /components - The partial sections that build any page. Components can be configured for global use throughout the layouts and pages. This facilitates atomic design, which is my preferred way of building solutions.
